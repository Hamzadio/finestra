/**
* @brief Fonction des boutons
* @details  Ce fichier contien la definition des fonctions utilisées
* @author HMZ LRB
* @date  1/02/24
* @version 0.9
* @file dialog.h
*
*/
#ifndef DIALOG_H
#define DIALOG_H

#include "qlcdnumber.h"
#include <QDialog>
#include <QPushButton>
#include <QLCDNumber>

class Dialog : public QDialog
{

private:
    Q_OBJECT
    QPushButton degriser;
    QPushButton Reveal;
    QPushButton Quit; //Widget buttonquiter
    QLCDNumber number ;
    QPushButton Bin;
    QPushButton Deci;
    QPushButton Hex;
    QPushButton plus;
    QPushButton minus;

    int numero = 0;

    void incrementer();
    void decrementer();
    void degrisermoins();

public:
    Dialog(QWidget *parent = nullptr); //create the windows nullptr(dosent have a parent so it the main windows)
    ~Dialog(); // destory the windows
};
#endif // DIALOG_H
